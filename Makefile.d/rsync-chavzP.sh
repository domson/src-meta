#!/bin/bash

for CONF in "$@"; do
	[ ! -f "$CONF" ] && continue
	TARGET=$(basename "${CONF%.conf}")
	while read var value; do
		declare "$var"="$value"
	done < <(cat "$CONF" | grep -v "^#")
	
	[ "$include" ] && rsync_opts+=(--include="$include")
	
	echo rsync -chavzP ${rsync_opts[@]} --stats $url::kdeftp "$TARGET"
done
