
PATH := $(PATH):src-meta/Makefile.d
srcmeta_CONF = src-meta/download.kde.org

export PATH

$(srcmeta_CONF): % : %.conf
	rsync-chavzP.sh "$^"

src-meta: $(srcmeta_CONF)
